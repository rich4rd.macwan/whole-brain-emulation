:title: Whole Brain Emulation
:author: Richard Macwan, Roberto Marroquin
:description: A presentation for the Local Culture module at Heriot Watt University
:keywords: presentation, whole brain emulation, mind substrate transfer
:css: presentation.css
:skip-help: true

----

Whole Brain Emulation
=====================

Roberto Marroquin and Richard Macwan
------------------------------------
    
.. image:: line.png 

Local Culture Presentation
....................................

Heriot Watt University
**********************
----

Outline
=======

- What is Whole Brain Emulation

 - Current reaserch

 - Is it even possible?

 - Technical aspects

- Ethical Issues

 - Experimentation

 - Moral status

 - New ethical principles

 - Identity Issues

----

:data-x: r1500

Whole Brain Emulation
=====================
or Mind Uploading, What is it?
------------------------------
- A hypothetical process of copying mental content(memory as well as "self") to a sufficiently complex computation device
- The copied entity would essentially be identical to the original *[Bamford],[Goertzel]*

.. .. raw:: html
    
..     <img width="240" height="180" src="presentation/animation.gif" style="position:relative;bottom:10%x"/>
    

----

:data-x: r2000

Current State of WBE
====================

- Neural prosthesis has a long history
- More prevalent in pop culture and fictional novels than in science

.. ----

.. :data-y: r1500

.. However...
.. ==========
.. Developments in the past century
.. -----------------------------------------
.. - Artificial cardiac pacemakers [#]_.
.. - Cochlear implants [#]_
.. - Deep-brain simulators[RKumar_] 
.. - Recordings from the motor cortex for control [Taylor_] and vision restoration [Hetling_] 


----

Current Research
=================
- Simulation of C.elegans round worm(302 neurons) *[Open Worm Project]*
- Simulation of Drosophilia fruit fly neural system *[Arena.P]* 
- Replacement of neural functionality of certain portions of a rat's brain. *[Berger]* , *[Prueckl]*
- The Blue Brain project

----

:data-rotate-z: 90
:data-scale: 5
:data-y: r8000

Is it possible?
================
Computational Complexities
--------------------------

- Very difficult to quantify actual computational requirements
- "According to Moore's Law mind uploading may be possible within decades" *[News Article]*

----

Technical approaches
====================
How can it be done?
--------------------
- Reconstruction from Behaviour
- Reconstruction from scan
- On-line approach

----

:data-y: r0
:data-x: r-5000


Reconstruction from Behaviour
=============================
- Loosely-Coupled Off-Loading (LCOL) *[Bainbridge] and [Rothblatt]*
    - a *Bottom-up* approach
    - take advantages of self-reports, lifelogs and video recordings that can be analyzed by an AI

----


Reconstruction from scan
========================

- Off-line approach *[Sandberg & Bostrom]*, *[Hayworth,2010]*
    - a *Top-down* approach
    - Deep scan of the neural and synaptic function leading to a functional knowledge of the system.
    - Most likely a destructive technique

----

On-line approach
====================

- Gradual replacement of the biological parts with artificial ones *[Bamford]*, *[Sotala]*


----

:data-rotate-z: 0
:data-scale: 1
:data-y: r5500

Ethical Issues
=============================
- In order to do good we need to have some ideas of what the good is and how to pursue it in the right way *[Sandberg, A]*
- To what extent are brain emulations moral?
- What new ethical concerns are introduced as a result of brain emulation technology?

.. note::

    - Ethics matters because we want to do good.

    - Considering potential risks and their ethical impacts even when dealing with merely possible future technologies. 
      The impact of a technology can be reduce by taking proactive steps in the present. 
      In many cases, the steps are simply to gather better information and have a
      few preliminary guidelines ready if the future arrives surprisingly early. While we have little
      information in the present, we have great leverage over the future. When the future arrives we
      may know far more, but we will have less ability to change it.

----

:data-y: r0
:data-x: r1500

Ethical Issues
==============

- Experimentation

- Moral Status

- New Ethical Principles

- Identity-Issues


----


Experimentation
===============
- Test Animals
    
    - Studying their neuroscience

    - Scanning Modalities

.. note::


    Brain emulations might require significant use of test animals to develop the technology.

    The only known methods able to generate complete brain scans are destructive


----


Moral Status
============

What gives moral status?

- **Sentience**: capacity for phenomenal experience, such as the 
  capacity to feel pain and suffer 

- **Sapience**: capacities associated with higher intelligence, 
  such as self‐awareness and being a reason‐responsive agent

Emulations have the same moral status than us?

.. note::

    - Two criteria are commonly proposed as being importantly linked to moral status
     
    - Our duties towards animals are merely indirect duties towards humanity. Animal nature has
      analogies to human nature, and by doing our duties to animals in respect of manifestations of human
      nature, we indirectly do our duty to humanity . . . . We can judge the heart of a man by his treatment
      of animals. (Regan & Singer, 1989, pp. 23 – 24)


----



New Ethical Principles
======================

What to do in a situation of moral uncertainty about the status of emulations?

- Principle of **Assuming the Most** (PAM) *[A.Sandberg]*

- Principle of **Substrate Non-Discrimination** *[N.Bostrom & Yudkowsky]*

- Principle of **Ontogeny Non‐Discrimination**  *[N.Bostrom & Yudkowsky]*

.. note::
    - PAM: Assume that any emulated system could have the same mental properties as the original system and treat it correspondingly 
    - PSD: If two beings have the same functionality and the same conscious experience, and differ only in the substrate of their 
      implementation, then they have the same moral status  
    - POD: If two beings have the same functionality and the same consciousness experience, and differ only in how they came into existence, 
      then they have the same moral status 


----

Identity-Issues
===============

- Multiple Copies

  - Properties?

  - Titles?

  - Criminal Records?

  - Responsibility?

*[A.Sandberg 2014]*

 


.. note::

        Personal identity is likely going to be a major issue

    These issues 
    will lead to major legal, social and political changes if they become
    relevant.



----

:data-y: r1500

Conclusions
===========
- Create systems that closely imitate real biological organisms completely

- Will have to deal with complex ethical, computational and philosophical issues

- Will be possible in the near future

----

Conclusions
===========
- Important to think about the impact of a technology before so that we can mitigate the possible problems

- When fully realized, will imply many legal, social and political changes


----


References
=================
 [1] Arena, P. et al. (2010); Dipt. di Ing. Elettr., Elettron. e dei Sist., Univ. degli Studi di Catania, Catania, Italy ; Patane, L. ; Termini, P.S., An insect brain computational model inspired by Drosophila melanogaster: Simulation results
 
 [2] Anders Sandberg (2014) Ethics of brain emulations, Journal of Experimental & Theoretical Artificial Intelligence, 26:3, 439-457, 
 
 [3] W Bainbridge.(2009) Religion for a galactic civilization 2.0.http://ieet.org/index.php/IEET/more/bainbridge20090820/.
 
----

References
=================

 [4] Bamford: Sim <http://www.sim.me.uk/neural/JournalArticles/Bamford2012IJMC.pdf>

 [5] Ben Goertzel, Human-level artificial general intelligence and the possibility of a technological singularity: A reaction to Ray Kurzweil's The Singularity Is Near, and McDermott's critique of Kurzweil

 [6] Blue Brain Project:<http://bluebrain.epfl.ch/>  

----

References
=================

 [7] Bostrom, N., & Yudkowsky, E. (2014). The ethics of artificial intelligence. In W. Ramsey & K. Frankish (Eds.), Cambridge handbook of artificial intelligence. Cambridge: Cambridge University Press

 [8] News Article,<http://news.bbc.co.uk/1/hi/8164060.stm>
 
 [9] Open Worm Project, http://www.openworm.org

----

References
=================

 [10] Oto, Degrees of Clarity <http://degreesofclarity.com/writing/oto_mind_uploading.pdf>

 [11] Rothblatt, <http://www.cyberev.org/>

 [12] R Prueckl, S Bamford,et al.(2011) Behavioral rehabilitation of the eye closure reflex in senescent rats using a real-time biosignal acquisition system. In International Conference of the IEEE Engineering in Medicine and Biology Society (EMBC)

----

References
=================

 [13] Sandberg, A. & Bostrom, N. (2008): Whole Brain Emulation: A Roadmap, Technical Report #2008‐3, Future of Humanity Institute, Oxford University

 [14] Sotala, Kaj. (2012). “Advantages of Artificial Intelligences, Uploads, and Digital Minds.” International Journal of Machine Consciousness 4 (1): 275–291

 [15] WileyBlackwell (2013),The Transhumanist Reader: Classical and Contemporary Essays on the Science, Technology, and Philosophy of the Human Future, 

 