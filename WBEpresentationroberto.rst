:title: Whole Brain Emulation 
:author: Roberto Marroquin, Richard Macwan
:description: Presentation on Whole Brain Emulation (WBE) for Local Culture lecture.



-----

Ethics on WBE
=============
In order to do good we need to have some ideas of
what the good is and how to pursue it in the right way [*A.Sandberg 2014*]


To what extent brain emulations are moral?

What new ethical concerns are introduced as a result of brain emulation technology?

.. note::

	- Ethics matters because we want to do good.

	- Considering potential risks and their ethical impacts even when dealing with merely possible future technologies. 
	  The impact of a technology can be reduce by taking proactive steps in the present. 
	  In many cases, the steps are simply to gather better information and have a
	  few preliminary guidelines ready if the future arrives surprisingly early. While we have little
	  information in the present, we have great leverage over the future. When the future arrives we
	  may know far more, but we will have less ability to change it.

----

Ethics on WBE
=============

-Experimentation

-Moral Status

-New Ethical Principles

-Identity

-Global Issues


----

Experimentation
===============
- Test Animals
	
	- Studying their neuroscience

	- Scanning Modalities

	- Suffering and Euthanasia


- Imposible to prove that the emulation reflects how 
  the real system operates [*Zeigler, 1985; Zeigler, Praehofer, & Kim, 2000*]

.. note::

	Brain emulations might require significant use of test animals to develop the technology.

	They would be necessary not only for direct scanning into emulations, but in various experiments gathering the necessary
	understanding of neuroscience, testing scanning modalities and comparing the real and
	simulated animals

	the only known methods able to generate complete data at cellular and
	subcellular resolution are destructive

	There could exist hidden properties that only very rarely come into play that are not
	represented.


----

Moral Status
============

What attributes ground moral status?

- **Sentience**: capacity for phenomenal experience, such as the 
  capacity to feel pain and suffer 

- **Sapience**: capacities associated with higher intelligence, 
  such as self‐awareness and being a reason‐responsive agent

Emulations have the same moral status than us?

[*N.Bostrom 2011*] 

.. note::

	- While it is fairly consensual that present‐day AI systems lack moral status, it is unclear 
	  exactly what attributes ground moral status.  Two criteria are commonly proposed as 
	  being importantly linked to moral status, either separately or in combination: sentience 
	  and sapience (or personhood).  These may be characterized roughly as follows: 
	 
	 
	- One common view is that many animals have qualia and therefore have some moral 
	  status, but that only human beings have sapience, which gives them a higher moral 
	  status than non‐human animals.

	- Our duties towards animals are merely indirect duties towards humanity. Animal nature has
	  analogies to human nature, and by doing our duties to animals in respect of manifestations of human
	  nature, we indirectly do our duty to humanity . . . . We can judge the heart of a man by his treatment
	  of animals. (Regan & Singer, 1989, pp. 23 – 24)


----

New Ethical Principles
======================

What to do in a situation of moral uncertainty about the status of emulations?

- Principle of **Assuming the Most** (PAM) [*A.Sandberg 2014*]

- Principle of **Substrate Non-Discrimination** [*N.Bostrom & Yudkowsky, 2014*]

- Principle of **Ontogeny Non‐Discrimination** [*N.Bostrom 2011*] 



.. note::

	- PAM: Assume that any emulated system could have the same mental properties as the original system and treat it correspondingly 

    - PSD: If two beings have the same functionality and the same conscious experience, and differ only in the substrate of their 
      implementation, then they have the same moral status  

	- POD: If two beings have the same functionality and the same consciousness experience, and differ only in how they came into existence, 
	  then they have the same moral status 


----

Identity
========

- Multiple Copies

  - Marriage?

  - Properties?

  - Titles?

  - Criminal Records?

  - Responsability?

  - Limit of copies?

[*A.Sandberg 2014*]

 


.. note::

	Personal identity is likely going to be a major issue

    Obviously, multiple copies of the same original person pose intriguing legal challenges. For
    example, contract law would need to be updated to handle contracts where one of the parties is
    copied – does the contract now apply to both? What about marriages? Are all copies descended
    from a person legally culpable of past deeds occurring before the copying? To what extent does
    the privileged understanding copies have of each other affect their suitability as witnesses
    against each other? How should votes be allocated if copying is relatively cheap and persons can
    do ‘ballot box stuffing’ with copies? Do copies start out with equal shares of the original’s
    property?

	These issues 
	will lead to major legal, social and political changes if they become
	relevant.


----

Global Issues
=============

- Potencial destabilising effects

  - Increasing Inequality

  - Marginalization

  - Racism and Xenophobic prejudices

  - Vulnerability of emulations



.. note::
	
	WBE could introduce numerous destabilising effects, such as increasing inequality, groups that become marginalised, disruption of existing social power relationships and the creation of opportunities to establish new kinds of power, the creation of situations in which the scope of human rights and property rights are poorly defined and subject to dispute, and particularly strong triggers for racist and xenophobic prejudices, or vigorous religious objections. While all these factors are speculative and depend on details of the world and WBE emergence scenario, they are a cause for concern.

    Brain emulations are extremely vulnerable by default: the software and data constituting them
    and their mental states can be erased or changed by anybody with access to the system on which 
    they are running. Their bodies are not self-contained and their survival is dependent upon
    hardware they might not have causal control over. They can also be subjected to undetectable
    violations such as illicit copying. From an emulation perspective, software security is identical
    to personal security.
    Emulations also have a problematic privacy situation, since not only their behaviour can be
    perfectly documented by the very system they are running on, but also their complete brain
    states are open for inspection

----

Conclusions
===========
The aim of brain emulation is to create systems that closely imitate real biological organisms in
terms of behaviour and internal causal structure


.. note::
	k

----

References
==========
